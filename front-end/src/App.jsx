import './App.css'
import React from 'react';
import MainPages from './components/Pages';
import { BrowserRouter as Router } from 'react-router-dom';

function App() {

  return (

    <Router>
      <div className="App">
        <MainPages />
      </div>
    </Router>
  )
}

export default App
