import * as React from "react";
import { ApiUser } from "./api";
import { message } from 'antd'
import { useCookie } from 'react-use'
import Cookies from "js-cookie";

export const GlobalState = React.createContext();
const accessToken = Cookies.get("accessToken")
export const DataProvider = ({ children }) => {
    const [refresh_Token, upDateRefreshToken] = useCookie("refreshToken");
    const [token, setToken] = React.useState(accessToken);
    const [user, setUser] = React.useState(null);

    React.useLayoutEffect(() => {
        (async () => {
            try {
                if (token) {
                    const result = await ApiUser.getProfile();
                    setUser(result);
                }
            } catch (error) {
                message.error(error.response.data.msg);
            }
        })();

    }, [token])

    const state = {
        token: [token, setToken],
        user: [user, setUser],
    };

    return <GlobalState.Provider value={state}>{children}</GlobalState.Provider>;
}