import { axiosClient } from "./client-axios";

// const domain = "http://localhost:5000";
const domain = "https://vinhnguyen.gcalls.vn";

export const ApiUser = {
    getProfile: () => {
        const url = `${domain}/user/info`;
        return axiosClient.get(url);
    },
    login: (data) => {
        const url = `${domain}/user/login`;
        return axiosClient.post(url, data);
    },
    register: (data) => {
        const url = `${domain}/user/register`;
        return axiosClient.post(url, data);
    },
    logout: () => {
        const url = `${domain}/user/logout`;
        return axiosClient.get(url);
    },
    refreshToken: (refreshToken) => {
        const url = `${domain}/user/refresh_token`;
        return axiosClient.post(url, refreshToken);
    }
};
