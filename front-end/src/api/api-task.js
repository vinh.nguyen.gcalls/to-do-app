import { axiosClient } from "./client-axios";

// const domain = "http://localhost:5000";
const domain = "https://vinhnguyen.gcalls.vn";

export const ApiTask = {
    getTask: () => {
        const url = `${domain}/task/`;
        return axiosClient.get(url);
    },
    addTask: (data) => {
        const url = `${domain}/task/add`;
        return axiosClient.post(url, data);
    },
    deleteTask: (id) => {
        const url = `${domain}/task/delete/${id}`;
        return axiosClient.delete(url);
    },
    deleteAllTask: () => {
        const url = `${domain}/task/deleteAll`;
        return axiosClient.delete(url);
    },
    deleteAllTaskCompleted: () => {
        const url = `${domain}/task/deleteAllCompleted`;
        return axiosClient.delete(url);
    },
    deleteAllTaskByStatus: (status) => {
        const url = `${domain}/task/deleteAllByStatus/${status}`;
        return axiosClient.delete(url);
    },
    updateTask: (id, data) => {
        const url = `${domain}/task/update/${id}`;
        return axiosClient.put(url, data);
    },
    updateTaskStatus: (id, data) => {
        const url = `${domain}/task/updateStatus/${id}`;
        return axiosClient.patch(url, data);
    }
}
