import React, { useContext } from "react";
import { Route, Routes, Router } from "react-router-dom";
import Login from "./auth/Login";
import Register from "./auth/Register";
import TaskPage from "./task/TaskPage";
import NotFound from "./NotFound";
import { GlobalState } from "../GlobalState";

function Pages() {

    const state = useContext(GlobalState);
    const [token] = state.token;
    // console.log('token', token)

    const CheckToken = (Element) => {
        if (token) {
            return <TaskPage />
        } else {
            return <Element />
        }
    };

    return (
        <Routes>
            <Route path="/" element={CheckToken(Login)} />
            <Route path="/login" element={CheckToken(Login)} />
            <Route path="/register" element={CheckToken(Register)} />
            <Route path="*" element={<NotFound />} />
        </Routes>

    );
}

export default Pages;