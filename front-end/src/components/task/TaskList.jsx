import React, { useEffect, useState } from "react";
import dayjs from "dayjs";
import { GlobalState } from "../../GlobalState";
import { Card, Button, Modal, Select, Popconfirm, message, Space, Table, Tag, Checkbox } from 'antd';
import { PlusOutlined } from '@ant-design/icons'
import { ApiTask } from "../../api";
import AddTask from "./AddTask";


function TaskList() {
    const state = React.useContext(GlobalState);
    const [token, setToken] = state.token;
    const [data, setData] = useState([]);
    const [list, setList] = useState([]);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [statusFilter, setStatusFilter] = useState("all task");
    const [currentTask, setCurrentTask] = useState(null);
    const handleGetTask = React.useCallback(() => token && ApiTask.getTask(), [token]);
    // console.log('token', token)
    useEffect(() => {
        (async () => {
            try {
                const result = await handleGetTask();
                setData(result);
                setList(result);
                // console.log('result', result)
            } catch (error) {
                console.log('error', error);
            }
        })();
    }, [token]);

    // Show Modal
    const showModal = () => {
        setIsModalOpen(true);
        setCurrentTask(null);
    };

    // Edit
    const showEditModal = (task) => {
        setCurrentTask(task);
        setIsModalOpen(true);
    };

    // after add task update state
    const addTask = (newTask) => {
        setData([...data, newTask.task]);
        setCurrentTask(null);
    };

    // after update task update state
    const updateTask = (updatedTask) => {
        setData(data.map(task => {
            if (task._id === updatedTask.task.id) {
                return { ...task, ...updatedTask.task };
            }
            return task;
        }));
        setCurrentTask(null);
    };

    // Delete
    const confirm = (id) => async () => {
        try {
            await ApiTask.deleteTask(id);
            message.success('Delete success');
            // render only the remaining tasks
            const filteredData = data.filter((item) => item._id !== id);
            setData(filteredData);
        } catch (error) {
            console.log('error', error);
        }
    };
    const cancel = (e) => {
        // console.log(e);
        // message.error('Click on No');
    };

    // Delete all task by statusFilter
    const deleteTaskByStatusFilter = async () => {
        console.log('statusFilter', statusFilter)
        try {
            await ApiTask.deleteAllTaskByStatus(statusFilter);
            message.success('Delete success');
            // render only the remaining tasks
            const filteredData = data.filter((item) => item.status !== statusFilter);
            setData(filteredData);
        } catch (error) {
            console.log('error', error);
        }
    };

    // mobile
    const [isMobile, setIsMobile] = useState(false);

    useEffect(() => {
        const handleResize = () => {
            setIsMobile(window.innerWidth < 400);
        };

        window.addEventListener('resize', handleResize);
        handleResize();

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    // Filter
    const handleStatusFilterChange = async (value) => {
        // console.log(`selected ${value}`)
        const result = await handleGetTask();
        setData(result);
        setList(result);
        setStatusFilter(value);
    };

    useEffect(() => {
        if (statusFilter === "all task") {
            setData(list);
        } else {
            const filteredData = list.filter((item) => item.status === statusFilter);
            setData(filteredData);
        }
    }, [statusFilter, list]);

    // Status
    const toggleTaskStatus = async (id, status) => {
        try {
            const updatedStatus = status === 'completed' ? 'in progress' : 'completed';
            await ApiTask.updateTaskStatus(id, { status: updatedStatus });
            message.success('Update status successfully');
            setData(data.map(task => {
                if (task._id === id) {
                    return { ...task, status: updatedStatus };
                }
                return task;
            }));
        } catch (error) {
            console.log('error', error);
        }
    };

    const columns = [
        {
            title: '',
            dataIndex: 'status',
            key: 'status',
            render: (_, { status, _id }) => (
                <>
                    {
                        status === 'overdue' ? (
                            <Checkbox checked={false} disabled={true} />
                        ) : (
                            status === 'completed' ? (
                                <Checkbox checked={true} onChange={() => toggleTaskStatus(_id, status)} />
                            ) : (
                                <Checkbox checked={false} onChange={() => toggleTaskStatus(_id, status)} />
                            )
                        )
                    }
                </>
            ),
        },
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            render: (text) => <a>{text}</a>,
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            render: (_, { status }) => (
                <>
                    {status === 'overdue' && (
                        <Tag color="#f50" key={status}>
                            {status.toUpperCase()}
                        </Tag>
                    )}
                    {status === 'completed' && (
                        <Tag color="#87d068" key={status}>
                            {status.toUpperCase()}
                        </Tag>
                    )}
                    {status === 'in progress' && (
                        <Tag color="#FF9555" key={status}>
                            {status.toUpperCase()}
                        </Tag>
                    )}
                </>
            ),
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Priority',
            key: 'priority',
            dataIndex: 'priority',
            render: (_, { priority }) => (
                <>
                    {priority === 'emergency' && (
                        <Tag color="magenta" bordered={false} key={priority}>
                            {priority.charAt(0).toUpperCase() + priority.slice(1)}
                        </Tag>
                    )}
                    {priority === 'high' && (
                        <Tag color="red" bordered={false} key={priority}>
                            {priority.charAt(0).toUpperCase() + priority.slice(1)}
                        </Tag>
                    )}
                    {priority === 'medium' && (
                        <Tag color="orange" bordered={false} key={priority}>
                            {priority.charAt(0).toUpperCase() + priority.slice(1)}
                        </Tag>
                    )}
                    {priority === 'low' && (
                        <Tag color="green" bordered={false} key={priority}>
                            {priority.charAt(0).toUpperCase() + priority.slice(1)}
                        </Tag>
                    )}

                </>
            ),
        },
        {
            title: 'Due Date',
            key: 'dueDate',
            dataIndex: 'dueDate',
            render: (dueDate) => (
                <>
                    {dayjs(dueDate).format('DD/MM/YYYY HH:mm:ss A')}
                </>
            ),
        },
        {
            title: 'Action',
            key: 'action',
            render: (_, record) => (
                <Space size="middle">
                    <Button onClick={() => showEditModal(record)} className="text-blue-400 border-none shadow-none">Edit</Button>
                    <Popconfirm
                        title="Delete the task"
                        description="Are you sure to delete this task?"
                        onConfirm={confirm(record._id)}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                    >
                        <Button danger className="text-red-400 border-none shadow-none">Delete</Button>
                    </Popconfirm>
                </Space>
            ),
        },
    ];


    return (
        <div>
            <Card
                title={
                    <div className={`flex ${isMobile ? 'flex-col' : 'justify-between'} w-full`}>
                        <Button
                            className="bg-blue-600 flex items-center"
                            type="primary"
                            onClick={showModal}
                        >
                            <PlusOutlined />
                            Add Task
                        </Button>
                        <AddTask
                            isModalOpen={isModalOpen}
                            setIsModalOpen={setIsModalOpen}
                            addTask={addTask}
                            currentTask={currentTask}
                            setCurrentTask={setCurrentTask}
                            updateTask={updateTask}
                        />
                        <div className="flex items-center">
                            <Select
                                defaultValue="All Task"
                                // style={{
                                //     maxWidth: 120,
                                // }}
                                className="w-full block mr-2"
                                onChange={handleStatusFilterChange}
                                options={[
                                    {
                                        value: 'all task',
                                        label: 'All Task',
                                    },
                                    {
                                        value: 'in progress',
                                        label: 'In Progress',
                                    },
                                    {
                                        value: 'completed',
                                        label: 'Completed',
                                    },
                                    {
                                        value: 'overdue',
                                        label: 'Overdue',
                                    },
                                ]}
                            />
                            <Popconfirm
                                title={`Delete ${statusFilter} tasks`}
                                description={`Are you sure to delete all ${statusFilter} tasks?`}
                                onConfirm={deleteTaskByStatusFilter}
                                onCancel={cancel}
                                okText="Yes"
                                cancelText="No"
                                disabled={statusFilter === 'all task'}
                            >
                                <Button danger disabled={statusFilter === 'all task'}>
                                    Delete Tasks
                                </Button>
                            </Popconfirm>
                        </div>
                    </div>
                }
                className="text-left mb-3"
            // extra={
            // }
            >
                <Table
                    columns={columns}
                    dataSource={data.slice().reverse()}
                    pagination={{ pageSize: 5 }}
                    scroll={{ x: 800 }}
                    rowKey="_id" />
            </Card>
        </div>
    );
}

export default TaskList;