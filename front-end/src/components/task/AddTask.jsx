
import React, { useEffect } from 'react';
import dayjs from 'dayjs';
import { Button, Form, Input, DatePicker, Select, Modal, message } from 'antd';
import { ApiTask } from "../../api";

const layout = {
    labelCol: {
        span: 6,
    },
    wrapperCol: {
        span: 18,
    },
};

/* eslint-disable no-template-curly-in-string */
const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not a valid email!',
        number: '${label} is not a valid number!',
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};

// Date Picker
// eslint-disable-next-line no-unused-vars
const onChange = (value, dateString) => {
    // console.log('Selected Time: ', value);
    // console.log('Formatted Selected Time: ', dateString);
};
const onOk = (value) => {
    // console.log('onOk: ', value.toISOString());
};


const AddTask = ({ isModalOpen, setIsModalOpen, addTask, currentTask, updateTask }) => {

    const [form] = Form.useForm();
    useEffect(() => {
        if (currentTask) {
            form.setFieldsValue({
                title: currentTask.title,
                description: currentTask.description,
                dueDate: dayjs(currentTask.dueDate, "YYYY/MM/DD HH:mm:ss Z"),
                priority: currentTask.priority,
            })
        } else {
            form.resetFields();
        }
    }, [currentTask])
    // Modal
    const handleOk = () => {
        setIsModalOpen(false);
    };
    const handleCancel = () => {
        setIsModalOpen(false);
    };
    // Form
    const onFinish = async (data) => {
        // console.log(data);
        data.dueDate = data.dueDate.toISOString();
        try {
            let result;
            if (currentTask) {
                result = await ApiTask.updateTask(currentTask._id, data);
                message.success('Update task successfully!');
                updateTask(result);
            } else {
                result = await ApiTask.addTask(data);
                message.success('Add task successfully!');
                addTask(result);
            }
            handleOk();
        } catch (error) {
            console.log('error', error);
            message.error('Add task failed!');
        }
    };
    return (
        <Modal
            forceRender
            title={currentTask ? "Edit Task" : "Add Task"}
            open={isModalOpen}
            onOk={handleOk}
            footer={null}
            onCancel={handleCancel}>

            <Form
                form={form}
                {...layout}
                name="nest-messages"
                onFinish={onFinish}
                style={{
                    maxWidth: 600,
                }}
                validateMessages={validateMessages}
            >
                <Form.Item
                    name="title"
                    label="Title"
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="description"
                    label="Description"
                >
                    <Input.TextArea />
                </Form.Item>
                <Form.Item
                    name="dueDate"
                    label="Due Date"
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <DatePicker
                        showTime
                        format="YYYY/MM/DD HH:mm:ss"
                        onChange={onChange}
                        onOk={onOk}
                    />
                </Form.Item>
                <Form.Item
                    name="priority"
                    label="Priority"
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <Select
                        // defaultValue="Low"
                        style={{
                            maxWidth: 120,
                        }}
                        className="w-full block"
                        onChange={this}
                        options={[
                            {
                                value: 'low',
                                label: 'Low',
                            },
                            {
                                value: 'medium',
                                label: 'Medium',
                            },
                            {
                                value: 'high',
                                label: 'High',
                            },
                            {
                                value: 'emergency',
                                label: 'Emergency',
                            },
                        ]}
                    />
                </Form.Item>
                <Form.Item
                    wrapperCol={{
                        ...layout.wrapperCol,
                        offset: 8,
                    }}
                >
                    <Button onClick={handleCancel} style={{ marginRight: 8 }}>
                        Cancel
                    </Button>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    )
};
export default AddTask;