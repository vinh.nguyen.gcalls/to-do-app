import React, { useEffect, useLayoutEffect, useState } from "react";
import { Col, Row, Popconfirm, Button, Avatar, Drawer, List, Modal, message } from 'antd';
import { LogoutOutlined, MenuOutlined, ExclamationCircleFilled } from '@ant-design/icons';

import { ApiUser } from "../../api";
import { GlobalState } from '../../GlobalState';
import TaskList from "./TaskList";

import { useNavigate } from "react-router-dom";
import Cookies from "js-cookie";

function TaskPage() {
    const state = React.useContext(GlobalState);
    const [token, setToken] = state.token;
    const [user, setUser] = state.user;
    const navigate = useNavigate();

    //get current user
    useLayoutEffect(() => {
        const getUserProfile = async () => {
            try {
                const result = await ApiUser.getProfile();
                const userData = result;
                setUser(userData);
            } catch (error) {
                console.error('Error retrieving user profile:', error);
                // Xử lý lỗi tại đây
            }
        };
        getUserProfile();
    }, []);

    useLayoutEffect(() => {
        if (token) {
            navigate('/');
        }
    }, [token]);

    const onLogout = async () => {
        await ApiUser.logout();
        setToken(null)
        setUser(null)
        Cookies.remove('refreshToken')
        Cookies.remove('accessToken')
        message.success("logged out successfully");
    }

    const { confirm } = Modal;
    const showConfirm = () => {
        confirm({
            title: 'I will be so sad :(',
            icon: <ExclamationCircleFilled />,
            content: 'Are you sure log out?',
            onOk() {
                onLogout();
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    };

    //Mobile menu
    const [isMobile, setIsMobile] = useState(false);
    const [isMenuVisible, setIsMenuVisible] = useState(true);
    const containerStyle = {
        display: isMenuVisible ? 'block' : 'none',
    };

    const handleResize = () => {
        const windowWidth = window.innerWidth;
        setIsMobile(windowWidth <= 360);
    };

    useEffect(() => {
        handleResize();
        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    const [open, setOpen] = useState(false);
    const showDrawer = () => {
        setOpen(true);
    };
    const onClose = () => {
        setOpen(false);
    };


    if (!isMobile) {
        return (
            <Row>
                <Col span={24}>
                    <div className='flex justify-between items-center pb-10'>
                        <Avatar className="bg-sky-400">{user && user.name ? user.name.charAt(0).toUpperCase() : ''}</Avatar>
                        <h1 className='text-center text-2xl  font-extrabold uppercase text-blue-600'>To-Do App</h1>
                        <Button danger icon={<LogoutOutlined />} onClick={showConfirm} shape="round" className="flex items-center">Logout</Button>
                    </div>
                </Col>
                <Col span={24}>
                    <TaskList />
                </Col>
            </Row>
        );
    }

    return (
        <Row>
            <Col span={24} className='flex justify-between items-center pb-10'>
                <h1 className='text-center text-2xl  font-extrabold uppercase text-blue-600'>To-Do App</h1>
                <div>
                    <Button type="text" className="text-blue-500" icon={<MenuOutlined className="flex items-end" />} onClick={showDrawer}></Button>
                    <Drawer title={
                        <div>
                            <h1 className='text-center text-2xl  font-extrabold uppercase text-blue-600'>To-Do App</h1>
                        </div>
                    } placement="right" onClose={onClose} open={open}>
                        <List
                            itemLayout="horizontal"
                            className="ps-4"
                            header={
                                <div className="flex justify-start items-center">
                                    <Avatar className="bg-sky-400 mb-3">{user && user.name ? user.name.charAt(0).toUpperCase() : ''}</Avatar>
                                    <span className="ps-3 font-semibold text-sm inline-block">Hello, {user && user.name ? user.name : ''}</span>
                                </div>}
                            footer={
                                <Button danger icon={<LogoutOutlined />} onClick={showConfirm} shape="round" className="flex items-center">Logout</Button>
                            }

                        >
                            <List.Item>
                                Home
                            </List.Item>
                            <List.Item>
                                About
                            </List.Item>
                            <List.Item>
                                Contact
                            </List.Item>
                        </List>
                    </Drawer>
                </div>
            </Col>
            <Col span={24}>
                <TaskList />
            </Col>
        </Row>
    );
}

export default TaskPage;