import React from 'react';
import { Form, Input, Button, Checkbox, Divider } from 'antd';
import { ApiUser } from "../../api";
import { useNavigate } from "react-router-dom";
import { GlobalState } from "../../GlobalState";
import { message } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { Link } from "react-router-dom";
import Cookies from 'js-cookie';
// import { GoogleLogin } from 'react-google-login';
// import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
// import { GoogleOutlined, FacebookOutlined } from '@ant-design/icons';
// import { GoogleLoginButton, FacebookLoginButton } from "react-social-login-buttons";
// import { GoogleLoginButton as GoogleLoginButton2, FacebookLoginButton as FacebookLoginButton2 } from "react-social-login-buttons";

const Register = () => {
    const state = React.useContext(GlobalState);
    const [_, setToken] = state.token;
    const [form] = Form.useForm();

    // password not match confirm password
    const compareToFirstPassword = (_, value) => {
        if (value && value !== form.getFieldValue('password')) {
            return Promise.reject(new Error('Password not match!'));
        }
        return Promise.resolve();
    };

    const onFinish = async (data) => {
        try {
            const { accessToken } = await ApiUser.register(data)
            if (accessToken) {
                setToken(accessToken)
                Cookies.set('accessToken', accessToken)
            }
            message.success("Register successfully");
        } catch (error) {
            message.error(error.msg);
        }
    };

    // const responseGoogle = async (response) => {
    //     try {
    //         const { accessToken, refreshToken } = await ApiUser.loginWithGoogle(response.tokenId)
    //         if (accessToken) {
    //             setToken(accessToken)
    //             localStorage.setItem('accessToken', accessToken)
    //             localStorage.setItem('refreshToken', refreshToken)
    //             history.push('/')
    //         }
    //         message.success("Login successfully");
    //     } catch (error) {
    //         message.error(error.response.data.msg);
    //     }
    // }

    // const responseFacebook = async (response) => {
    //     try {
    //         const { accessToken, refreshToken } = await ApiUser.loginWithFacebook(response.accessToken)
    //         if (accessToken) {
    //             setToken(accessToken)
    //             localStorage.setItem('accessToken', accessToken)
    //             localStorage.setItem('refreshToken', refreshToken)
    //             history.push('/')
    //         }
    //         message.success("Login successfully");
    //     } catch (error) {
    //         message.error(error.response.data.msg);
    //     }
    // }

    return (
        <div className='flex justify-center items-center'>
            <Form
                form={form}
                size='large'
                name="normal_login"
                className="login-form"
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
            >
                <h1 className='text-center text-2xl pb-10  font-extrabold text-blue-600'>Welcome to To-Do App</h1>
                <Form.Item
                    name="name"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Name!',
                        },
                    ]}
                >
                    <Input prefix={<UserOutlined className="site-form-item-icon" />}
                        placeholder="Name"
                    />
                </Form.Item>

                <Form.Item
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Email!',
                        },
                    ]}
                >
                    <Input prefix={<UserOutlined className="site-form-item-icon" />}
                        placeholder="Email"
                    />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Password!',
                        },
                        {
                            min: 6,
                            message: 'Password must be at least 6 characters.'
                        }
                    ]}
                >
                    <Input.Password
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        placeholder="Password"
                    />
                </Form.Item>


                <Form.Item
                    name="confirmPassword"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Password!',
                        },
                        {
                            min: 6,
                            message: 'Password must be at least 6 characters.'
                        },
                        {
                            validator: compareToFirstPassword,
                            message: 'Password not match!'
                        }
                    ]}
                >
                    <Input.Password
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        placeholder="Confirm Password"
                    />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button w-full">
                        Register
                    </Button>
                    <div className='flex justify-center items-center pt-5'>
                        <Divider plain><Link to='/login' className='text-blue-600'>Already have an account? Login</Link></Divider>
                    </div>
                </Form.Item>
            </Form>
        </div>
    );
};

export default Register;