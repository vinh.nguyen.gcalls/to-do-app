import * as React from 'react';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input, Divider, message } from 'antd';
import { ApiUser } from "../../api";
import { GlobalState } from "../../GlobalState"
import { Link } from "react-router-dom";
import Cookies from 'js-cookie';


function Login() {

    const state = React.useContext(GlobalState);
    const [_, setToken] = state.token;


    const onFinish = async (data) => {
        try {
            const { accessToken, refreshToken } = await ApiUser.login(data)
            if (accessToken) {
                setToken(accessToken)
                Cookies.set('refreshToken', refreshToken)
                Cookies.set('accessToken', accessToken)
            }
            message.success("Login successfully");
        } catch (error) {
            console.log('error:', error.msg);
            message.error(error.msg);

        }
    };

    return (
        <div className='flex justify-center items-center'>
            <Form
                size='large'
                name="normal_login"
                className="login-form"
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
            >
                <h1 className='text-center text-2xl pb-10  font-extrabold text-blue-600'>Welcome to To-Do App</h1>
                <Form.Item
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Email!',
                        },
                    ]}
                >
                    <Input prefix={<UserOutlined className="site-form-item-icon" />}
                        placeholder="Email"
                    />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Password!',
                        },
                    ]}
                >
                    <Input.Password
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        placeholder="Password"
                    />
                </Form.Item>
                <Form.Item>
                    <Form.Item name="remember" valuePropName="checked" noStyle>
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item>

                    <a className="login-form-forgot text-blue-400" href="#">
                        Forgot password
                    </a>
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit" size='large' className="login-form-button bg-blue-600 w-full">
                        Log in
                    </Button>
                    <Divider plain>You don't have an account? <Link to="/register" className='text-blue-400'>Register now!</Link></Divider>
                </Form.Item>
            </Form>
        </div>
    );
};
export default Login;