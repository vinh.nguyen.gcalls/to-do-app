const router = require('express').Router();
const userCtrl = require('../controllers/userCtrl');
const auth = require('../middleware/auth');


router.get('/info', auth, userCtrl.getUser);

router.post('/register', userCtrl.register);

router.post('/login', userCtrl.login);

router.get('/logout', userCtrl.logout);

router.post('/refresh_token', userCtrl.refreshToken);



module.exports = router;