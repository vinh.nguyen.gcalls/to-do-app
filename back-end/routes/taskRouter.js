const router = require('express').Router();
const taskCtrl = require('../controllers/taskCtrl');
const auth = require('../middleware/auth');
const checkTaskOwnership = require('../middleware/checkOwnership');


router.get('/', auth, checkTaskOwnership, taskCtrl.getUserTasks);

router.post('/add', auth, taskCtrl.addUserTask);

router.delete('/delete/:id', auth, checkTaskOwnership, taskCtrl.deleteUserTask);

router.delete('/deleteAll', auth, checkTaskOwnership, taskCtrl.deleteAllUserTasks);

router.delete('/deleteAllCompleted', auth, checkTaskOwnership, taskCtrl.deleteAllUserTasksCompleted);

router.delete('/deleteAllByStatus/:status', auth, checkTaskOwnership, taskCtrl.deleteAllUserTasksByStatus);

router.put('/update/:id', auth, checkTaskOwnership, taskCtrl.updateUserTask);

router.patch('/updateStatus/:id', auth, checkTaskOwnership, taskCtrl.updateUserTaskStatus);



module.exports = router;