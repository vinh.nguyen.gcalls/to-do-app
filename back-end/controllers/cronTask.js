const cron = require('cron');
const moment = require('moment');
const Task = require('../models/taskModel');

const cronTask = {
    // This function will be called every minute
    // and will check if there are any tasks that are due
    // and will update their status to "overdue".
    checkDueDate: new cron.CronJob('* * * * *', async () => {
        try {
            const tasks = await Task.find({ status: "in progress" });
            tasks.forEach(async task => {
                if (moment(task.dueDate).isBefore(moment())) {
                    await Task.findOneAndUpdate({ _id: task._id }, { status: "overdue" });
                }
            });
        } catch (err) {
            console.log(err);
        }
    }
    )
}

module.exports = cronTask;