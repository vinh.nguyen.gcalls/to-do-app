const Users = require('../models/userModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const userCtrl = {
    register: async (req, res) => {
        try {
            const { name, email, password, phone } = req.body;

            const user = await Users.findOne({ email });
            if (user) return res.status(401).json({ msg: "The email already exists." });

            if (password.length < 6)
                return res.status(401).json({ msg: "Password is at least 6 characters long." });

            // Password Encryption
            const passwordHash = await bcrypt.hash(password, 10);
            const newUser = new Users({
                name, email, password: passwordHash, phone
            });

            // Save mongodb
            await newUser.save();

            // Then create jsonwebtoken to authentication
            const accessToken = createAccessToken({ id: newUser._id });
            const refreshToken = createRefreshToken({ id: newUser._id });

            res.cookie('refreshToken', refreshToken, {
                httpOnly: true,
                path: '/',
                maxAge: 7 * 24 * 60 * 60 * 1000 // 7d
            });

            res.json({ accessToken, newUser });
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },
    login: async (req, res) => {
        try {
            const { email, password } = req.body;

            const user = await Users.findOne({ email });
            if (!user) return res.status(400).json({ msg: "User does not exist." });

            const isMatch = await bcrypt.compare(password, user.password);
            if (!isMatch) return res.status(401).json({ msg: "Incorrect password." });

            // If login success, create access token and refresh token
            const accessToken = createAccessToken({ id: user._id });
            const refreshToken = createRefreshToken({ id: user._id });

            // res.cookie('refreshToken', refreshToken, {
            //     httpOnly: true,
            //     path: '/',
            //     maxAge: 7 * 24 * 60 * 60 * 1000 // 7d
            // });

            res.json({ accessToken, refreshToken });

        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    logout: async (req, res) => {
        try {
            res.clearCookie('refreshToken', { path: '/' });
            return res.json({ msg: "Logged out" });
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    getUser: async (req, res) => {
        try {
            const user = await Users.findById(req.user.id).select('-password');
            if (!user) return res.status(400).json({ msg: "User does not exist." });

            res.json(user);
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    refreshToken: (req, res) => {
        try {
            const { refreshToken } = req.body;
            // const refreshToken = req.cookies.refreshToken;
            if (!refreshToken) return res.status(400).json({ msg: "Please Login or Register!" });

            jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
                if (err) return res.status(400).json({ msg: "Please Login or Register" });

                const accessToken = createAccessToken({ id: user.id });

                res.json({ accessToken });
            });

        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    }
};

const createAccessToken = (user) => {
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '30m' });
}

const createRefreshToken = (user) => {
    return jwt.sign(user, process.env.REFRESH_TOKEN_SECRET, { expiresIn: '7d' });
}

module.exports = userCtrl;