const Task = require('../models/taskModel');

const taskCtrl = {

    getAllTasks: async (req, res) => {
        try {
            const tasks = await Task.find();
            res.json(tasks);
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    getUserTasks: async (req, res) => {
        try {
            const tasks = await Task.find({ createdBy: req.user.id });
            res.json(tasks);
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    getUserTaskById: async (req, res) => {
        try {
            const task = await Task.findOne({ _id: req.params.id, createdBy: req.user.id });
            if (!task) {
                return res.status(404).json({ msg: "Task not found or you are not the creator." });
            }
            res.json(task);
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    addUserTask: async (req, res) => {
        try {
            const { title, description, status, priority, dueDate } = req.body;
            const newTask = new Task({
                title, description, status, priority, dueDate, createdBy: req.user.id
            });

            await newTask.save();

            res.json({ msg: "Created a Task", task: newTask });
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    deleteUserTask: async (req, res) => {
        try {
            const task = await Task.findOneAndDelete({ _id: req.params.id, createdBy: req.user.id });
            if (!task) {
                return res.status(404).json({ msg: "Task not found or you are not the creator." });
            }
            res.json({ msg: "Deleted a Task" });
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    deleteAllUserTasks: async (req, res) => {
        try {
            await Task.deleteMany({ createdBy: req.user.id });
            res.json({ msg: "Deleted all tasks" });
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    deleteAllUserTasksCompleted: async (req, res) => {
        try {
            await Task.deleteMany({ createdBy: req.user.id, status: "Completed" });
            res.json({ msg: "Deleted all completed tasks" });
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    deleteAllUserTasksByStatus: async (req, res) => {
        try {
            await Task.deleteMany({ createdBy: req.user.id, status: req.params.status });
            res.json({ msg: "Deleted all tasks with status " + req.params.status });
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    updateUserTask: async (req, res) => {
        try {
            const { title, description, status, priority, dueDate } = req.body;

            await Task.findOneAndUpdate({ _id: req.params.id, createdBy: req.user.id }, {
                title, description, status, priority, dueDate
            });

            res.json({
                msg: "Updated a Task",
                task: { id: req.params.id, title: title, description: description, status: status, priority: priority, dueDate: dueDate }
            });
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    updateUserTaskStatus: async (req, res) => {
        try {
            const { status } = req.body;
            console.log(status)
            if (status !== 'overdue') {
                await Task.findOneAndUpdate({ _id: req.params.id, createdBy: req.user.id }, {
                    status
                });
            }
            else {
                await Task.findOneAndUpdate({ _id: req.params.id, createdBy: req.user.id }, {
                    status: 'overdue',
                });
            }
            res.json({ msg: "Updated a status" });
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    }
}

module.exports = taskCtrl;