const Task = require('../models/taskModel');

const checkTaskOwnership = async (req, res, next) => {
    try {
        const taskId = req.params.taskId;
        const userId = req.user.id; // ID của người dùng hiện tại

        const task = await Task.findOne({ createdBy: userId });

        if (!task) {
            return res.status(404).json({ msg: "You are not the creator." });
        }

        next();
    } catch (err) {
        return res.status(500).json({ msg: err.message });
    }
};

module.exports = checkTaskOwnership;