require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const cronTask = require('./controllers/cronTask');

const port = process.env.PORT || 5000;
const app = express();
app.use(express.json());
app.use(cookieParser());
app.use(cors());

//Routes
app.use('/user', require('./routes/userRouter'));
app.use('/task', require('./routes/taskRouter'));

//Connect to MongoDB
const URI = process.env.MONGODB_URL;

const connectToDatabase = async () => {
    try {
        await mongoose.connect(URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log('Connected to MongoDB');
        // Start cron job after successfully connecting to MongoDB
        cronTask.checkDueDate.start();
    } catch (err) {
        console.error('Failed to connect to MongoDB:', err);
        // Handle error appropriately
    }
};

connectToDatabase();

// Serve static assets if in production
if (process.env.NODE_ENV === 'production') {
    // Set static folder
    app.use(express.static('../front-end/build'));

    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, '../front-end', 'build', 'index.html'));
    });
}

app.get('/', (req, res) => {
    res.json({ msg: "Welcome to my task manager app~!" });
});

app.listen(port, () => console.log(`Server is running on port ${port}`));